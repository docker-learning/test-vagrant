# -*- coding: utf-8 -*-
import re
import sys
import requests
import gitlab
from os.path import dirname
from os import path
from os import system

URL = 'http://gitlab.docker-learning.dev'
RESET_PASSWORD_URL = 'http://gitlab.docker-learning.dev/users/password/edit'
SIGN_IN_URL = 'http://gitlab.docker-learning.dev/users/sign_in'
LOGIN_URL = 'http://gitlab.docker-learning.dev/users/sign_in'
CHANGE_PASSWORD = 'http://gitlab.docker-learning.dev/users/password'

session = requests.Session()

response = session.get(SIGN_IN_URL)
sign_in_page = response.content

for l in sign_in_page.split('\n'):
    m = re.search('name="authenticity_token" value="([^"]+)"', l)
    if m:
        break

token = None
if m:
    token = m.group(1)

if not token:
    print('Unable to find the authenticity token')
    sys.exit(1)

data = {'user[login]': 'root',
        'user[password]': 'q1w2e3r4t5',
        'authenticity_token': token}
r = session.post(LOGIN_URL, data=data)
if r.status_code != 200:
    print('Failed to log in')
    sys.exit(1)

gl = gitlab.Gitlab(URL, private_token="fegx5oSDkxNxo2xbyx55", api_version=4, session=session)
auth = {
    'Authorization': 'fegx5oSDkxNxo2xbyx55'
}
gl.auth()

kh = open('/home/vagrant/.ssh/id_rsa.pub', 'r')

key = str(kh.read())

gl.user.keys.create({
    'key': key,
    'title': 'machine'
})

docker_learning = gl.groups.create({'name': "docker-learning", 'path': "docker-learning"}, headers=auth)
images = gl.groups.create({'name': "images", 'path': "images", 'parent_id': docker_learning.id}, headers=auth)

application = gl.projects.create({
    'name': 'application',
    'namespace_id': docker_learning.id
}, headers=auth)

swarm_vagrant = gl.projects.create({
    'name': 'swarm-vagrant',
    'namespace_id': docker_learning.id
}, headers=auth)

test_vagrant = gl.projects.create({
    'name': 'test-vagrant',
    'namespace_id': docker_learning.id
}, headers=auth)

nginx = gl.projects.create({
    'name': 'nginx',
    'namespace_id': images.id
}, headers=auth)

fpm = gl.projects.create({
    'name': 'fpm',
    'namespace_id': images.id
}, headers=auth)

mariadb = gl.projects.create({
    'name': 'mariadb',
    'namespace_id': images.id
}, headers=auth)

redis = gl.projects.create({
    'name': 'redis',
    'namespace_id': images.id
}, headers=auth)

command = """
git config --global user.name 'Docker Learning';
git config --global user.email 'user@docker-learning.dev';
cd {dir}/../projects/{dir_name};
git init .;
git add .gitignore; 
git commit -m 'init'; 
git add .; git commit -m 'add files';
git remote remove origin;
git remote add origin {url};
git push origin master -uf;
"""

projects = {
    'application': application.http_url_to_repo,
    'redis': redis.http_url_to_repo,
    'nginx': nginx.http_url_to_repo,
    'mariadb': mariadb.http_url_to_repo,
    'fpm': fpm.http_url_to_repo,
}

for dir, url in projects.iteritems():
    curDir = dirname(path.realpath(__file__))+'/'
    system(command
           .replace('{dir}', curDir)
           .replace('{dir_name}', dir)
           .replace('{url}', url.replace('http://', 'http://root:q1w2e3r4t5@'))
           )
