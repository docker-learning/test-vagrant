#!/usr/bin/env bash
alias vcomposer="composer --verbose --profile"
alias vphpunit="./vendor/phpunit/phpunit/phpunit "
alias treset="tput reset"
alias cleardocker="docker stop $(docker ps -a -q); docker rm -f $(docker ps -a -q); docker rmi -f $(docker images -q); docker system prune; docker images purge; docker network prune;"
alias vphpunit="./vendor/phpunit/phpunit/phpunit "
