#!/usr/bin/env bash
DIR=$(dirname "$(readlink -f "$0")")

wget https://releases.hashicorp.com/vagrant/2.0.2/vagrant_2.0.2_x86_64.deb
sudo sudo apt-get install -y virtualbox-guest-x11 virtualbox-guest-additions-iso
sudo dpkg -i vagrant_2.0.2_x86_64.deb
sudo apt-get install --fix-broken
rm -f vagrant_2.0.2_x86_64.deb
vagrant destroy -f
sudo rm -rf $DIR/projects/*
sudo rm -rf $DIR/.vagrant.d/*
sudo rm -rf $DIR/gitlab/gitlab*
vagrant box update
vagrant plugin repair
vagrant plugin update
vagrant plugin install vagrant-hostmanager
vagrant plugin install vagrant-vbguest
vagrant up

# Up swarm boxes
cd $DIR/projects/swarm-vagrant
vagrant destroy -f
vagrant up
cd $DIR/projects/swarm-vagrant-second
vagrant destroy -f
vagrant up
