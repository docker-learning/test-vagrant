# Please see the online documentation at
# https://docs.vagrantup.com.

# OPTIONS
Vagrant.configure("2") do |config|
  config.vm.post_up_message = "Done!"

  config.vm.provision "shell", path: "./install.sh"
  config.vm.provision "shell", path: "./run-gitlab.sh", run: 'always'

  config.vm.provider "virtualbox" do |vb|
    vb.gui = false
    vb.memory = 2000
    vb.cpus = 2
    vb.name = "test-docker"
  end

   config.vbguest.auto_update = true
   config.vbguest.auto_reboot = true

  config.vm.provision :hostmanager
  config.hostmanager.enabled = true
  config.hostmanager.manage_host = true
  config.hostmanager.manage_guest = true
  config.hostmanager.ignore_private_ip = false
  config.hostmanager.include_offline = true

  config.vm.network "private_network", ip: "192.168.152.133"

  config.vm.define 'docker-learning.dev' do |node|
    node.vm.hostname = 'git'
    node.vm.network :private_network, ip: '192.168.152.133'
    node.hostmanager.aliases = %w(docker-learning.dev gitlab.docker-learning.dev test-project-docker.dev registry.gitlab.docker-learning.dev)
  end

  required_plugins = %w( vagrant-vbguest vagrant-disksize )
  _retry = false
  required_plugins.each do |plugin|
    unless Vagrant.has_plugin? plugin
      system "vagrant plugin install #{plugin}"
      _retry=true
    end
  end

  if (_retry)
    exec "vagrant " + ARGV.join(' ')
  end

  config.vm.box = "debian/stretch64"
  config.vm.synced_folder "./", "/home/vagrant/docker", id: "vagrant", :nfs => false, owner: "vagrant", group: "vagrant"

  # fix "stdin: is not a tty"
  config.vm.provision "fix-no-tty", type: "shell" do |s|
    s.privileged = false
    s.inline = "sudo sed -i '/tty/!s/mesg n/tty -s \\&\\& mesg n/' /root/.profile"
  end
end
