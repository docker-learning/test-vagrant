#!/usr/bin/env bash

echo "Generate key for vagrant box:"

if [ ! -f "/home/vagrant/.ssh/id_rsa" ]; then
    sudo su -c "ssh-keygen -f /home/vagrant/.ssh/id_rsa -t rsa -N ''" -s /bin/bash vagrant
fi

sudo apt-get update -y
sudo apt-get install -y git apt-transport-https ca-certificates curl software-properties-common vim htop python-pip
sudo pip install docker-compose python-gitlab

# Install docker
sudo apt-get upgrade -y
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt-key fingerprint 0EBFCD88
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"
sudo apt-get update -y
sudo apt-get install -y docker-ce
sudo usermod -aG docker vagrant

# Create volumes folders
mkdir -p /home/vagrant/docker/projects/data/mariadb
mkdir -p /home/vagrant/docker/projects/data/app_logs
mkdir -p /home/vagrant/docker/projects/data/cache

# Clone test projects
declare -A dirs=( [application]="docker-learning" [mariadb]="docker-learning/images" [redis]="docker-learning/images" [swarm-vagrant]="docker-learning" )
cd /home/vagrant/docker/projects

for KEY in "${!dirs[@]}"
do
    if [ ! -d "/home/vagrant/docker/projects/${KEY}" ]; then
        git clone "https://gitlab.com/${dirs[$KEY]}/${KEY}.git"
    fi
done

declare -A dirs=( [fpm]="docker-learning/images" [nginx]="docker-learning/images" )
cd /home/vagrant/docker/projects

for KEY in "${!dirs[@]}"
do
    if [ ! -d "/home/vagrant/docker/projects/${KEY}" ]; then
        git clone "https://gitlab.com/${dirs[$KEY]}/${KEY}.git"
        cp -r application "${KEY}"
    fi
done

if [ ! -d "/home/vagrant/docker/projects/swarm-vagrant-second" ]; then
    git clone https://gitlab.com/docker-learning/swarm-vagrant.git ./swarm-vagrant-second
    rm -rf .git
    sed -i "s/config.vm.network \"private_network\", ip: \"192.168.152.234\"/config.vm.network \"private_network\", ip: \"192.168.2.5\"/" /home/vagrant/docker/projects/swarm-vagrant/Vagrantfile
    sed -i "s/config.vm.network \"private_network\", ip: \"192.168.152.234\"/config.vm.network \"private_network\", ip: \"192.168.2.7\"/" /home/vagrant/docker/projects/swarm-vagrant-second/Vagrantfile
    sed -i "s/vb.name = \"swarm-test-docker\"/vb.name = \"swarm-test-docker-second\"/" /home/vagrant/docker/projects/swarm-vagrant-second/Vagrantfile
    echo "cd /home/vagrant/docker" >> /home/vagrant/.bash_aliases
    chown vagrant:vagrant /home/vagrant/docker
    chown vagrant:vagrant /home/vagrant/.bash_aliases
    sudo su -c "echo \"127.0.0.1 docker-learning.dev gitlab.docker-learning.dev\" >> /etc/hosts" -s /bin/bash root
    sudo su -c "echo \"192.168.152.234 docker-learning.dev gitlab.docker-learning.dev\" >> /etc/hosts" -s /bin/bash root
fi

# Up gitlab
# Up nginx proxy
docker network create nginx-proxy-net
docker run --name nginx-proxy -p "80:80" -p "443:443" -v "/var/run/docker.sock:/tmp/docker.sock:ro" --net "nginx-proxy-net" -d jwilder/nginx-proxy

# Start container gitlab
sudo cp -r /home/vagrant/docker/gitlab /gitlab
sudo chown -R vagrant:vagrant /gitlab
docker-compose up -d --build
bash /home/vagrant/docker/run-gitlab.sh

# Change gitlab root password to q1w2e3r4t5
PASSWORD='\$\2a\$10\$\bZr1XwP8BmrG13Xdqk4Vv.00WlK7LXA1LPXggK6IroXrUqOi/EnVO'
sudo su -c "docker-compose exec gitlab gitlab-psql -d gitlabhq_production -c \"UPDATE users set password_automatically_set = false, encrypted_password = replace('${PASSWORD}', '\\', '') WHERE id = 1\"" -s /bin/bash vagrant

# Add personal privatge token for root
sudo su -c "docker-compose exec gitlab gitlab-psql -d gitlabhq_production -c \"INSERT INTO personal_access_tokens (user_id, token, name, revoked, created_at, updated_at, scopes) VALUES(1, 'fegx5oSDkxNxo2xbyx55', 'Full', 'f', '2018-03-13 12:27:34.972192', '2018-03-13 12:27:34.972192', '---        '||chr(10)||'- sudo'||chr(10)||'- api')\"" -s /bin/bash vagrant

sudo su -c "cp /home/vagrant/docker/.bash* /home/vagrant" -s /bin/bash vagrant

sudo su -c "python /home/vagrant/docker/export-projects/exports.py" -s /bin/bash vagrant
