#!/usr/bin/env bash

eval "$(ssh-agent)"
ssh-add -L
cd /gitlab
docker start nginx-proxy
docker-compose start
echo "Waiting start gitlab..."
until $(curl --output /dev/null --silent --head --fail http://gitlab.docker-learning.dev); do
    printf '.'
    sleep 2
done
